/*
 * Grid.h
 *
 *  Created on: Apr 19, 2014
 *      Author: berkay
 */

#ifndef GRID_H_
#define GRID_H_


#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>
#include <boost/geometry/multi/geometries/multi_polygon.hpp>

#include <boost/multi_array.hpp>
#include <list>

#include <stx/btree.h>
#include <stx/btree_map.h>
#include <stx/btree_multimap.h>


#include "Instance.h"


using namespace boost::geometry;
using namespace stx;

template <int Slots>
struct btree_traits_nodebug
{
	static const bool	selfverify = true;
	static const bool	debug = false;
    
	static const int 	leafslots = Slots;
	static const int	innerslots = Slots;
};


typedef model::d2::point_xy<double> point;
typedef model::d2::point_xy<int> gridIndex;
typedef model::polygon<model::d2::point_xy<double> > polygon;
typedef model::box< point > box;
typedef stx::btree_multimap<int, Instance, std::less<int>, btree_traits_nodebug<4> >	bp_tree;

class Grid {
public:
	Grid(double dimX, double dimY, int xCell, int yCell);
	std::list<gridIndex> findIntersectionCells(polygon geom);
	virtual ~Grid();
	bp_tree** bptrees;
	box** cells;
private:
	double dimX;
	double dimY;
	int xCell;
	int yCell;
    
	box grid;
    
};

#endif /* GRID_H_ */