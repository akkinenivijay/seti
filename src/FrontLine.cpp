#include "FrontLine.h"

unorderedFeatureSet currentInstancesInHash;
boost::hash<int> int_hash;

// Global static pointer used to ensure a single instance of the class.
FrontLine* FrontLine::frontlineInstance = NULL;

/** This function is called to create an instance of the class.
 Calling the constructor publicly is not allowed. The constructor
 is private and is only called by this Instance function.
 */
FrontLine* FrontLine::getInstance() {
	if (!frontlineInstance) {
		frontlineInstance = new FrontLine;
	}
    
	return frontlineInstance;
}


/**
 * The Code is commented because we are not using frontline in the experiments anymore.
 **/
void FrontLine::addFeatureInstance(Instance &instance, Grid &grid) {
    
//    if (currentInstancesInHash.find(instance) == currentInstancesInHash.end()) {
//        //std::cout << instance.instanceID << " not found" << std::endl;
//        currentInstancesInHash.insert(instance);
//        
//    } else {
//        
//        boost::unordered_set<Instance>::const_iterator got =
//        currentInstancesInHash.find(instance);
//        Instance prevInst = *got;
//        
//        currentInstancesInHash.erase(prevInst);
//        currentInstancesInHash.insert(instance);
//        
//    }
    
    numberOfInstancesInserted++;
    
    boost::timer t1;
    std::list<gridIndex> gridIndices = grid.findIntersectionCells(instance.polygon);
    double elapsed_time1 = t1.elapsed();
    intersectionTime = elapsed_time1 + intersectionTime;
    
    for (const auto& elem : gridIndices) {
        boost::timer t;
        grid.bptrees[elem.x()][elem.y()].insert2(instance.time, instance);
        double elapsed_time = t.elapsed();
		insertionTime = insertionTime + elapsed_time;
    }
    
}

void FrontLine::printHashSet() {
	std::cout << currentInstancesInHash.size() << std::endl;
}

//Hashvalue implementation
inline std::size_t hash_value(const Instance &ftr) {
	boost::hash<int> hasher;
	size_t rval = hasher(ftr.instanceID);
	return rval;
}

inline bool operator==(const Instance &a, const Instance &b) {
	return a.instanceID == b.instanceID;
}
