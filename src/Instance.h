/*
 * feature.h
 *
 *  Created on: Apr 19, 2014
 *      Author: berkay
 */

#ifndef INSTANCE_H_
#define INSTANCE_H_
#include <string>
#include <iostream>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>

class Instance
{
    
public:
    Instance();
    Instance(std::string _featureID, int _instanceID, int _time, std::string _polygonWKT);
    int getInstanceID();
    void printInstance();
    const std::string getFeatureID();
    int getTime();
    boost::geometry::model::polygon<boost::geometry::model::d2::point_xy<double> > getPolygon();
    int instanceID;
    boost::geometry::model::polygon<boost::geometry::model::d2::point_xy<double>> polygon;
    int time;
    
private:
    std::string featureID;
    
};
#endif /* INSTANCE_H_ */
