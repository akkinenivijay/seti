/*
 * Grid.cpp
 *
 *  Created on: Apr 19, 2014
 *      Author: Vijay Akkineni
 */

#include "Grid.h"

Grid::Grid(double dimX, double dimY, int xCell, int yCell) {
    
	this->dimX = dimX;
	this->dimY = dimY;
	this->xCell = xCell;
	this->yCell = yCell;
    
	point mincorner(0.0, 0.0);
	point maxcorner(dimX, dimY);
    
	box grid(mincorner, maxcorner);
	cells = new box*[xCell];
	bptrees = new bp_tree*[xCell];
	for (int i = 0; i < xCell; ++i){
		cells[i] = new box[yCell];
		bptrees[i] = new bp_tree[yCell];
	}
    
	for(int i = 0; i != xCell; ++i){
		for(int j = 0; j != yCell; ++j){
			point upperLeft((dimX / xCell) * i, (dimY / yCell) * j);
			point lowerRight((dimX / xCell) * (i+1), (dimY / yCell) * (j+1));
			box cell_ij( upperLeft, lowerRight );
			cells[i][j] = cell_ij;
		}
	}
	std::cout << dsv(grid) << std::endl;
	for(int i = 0; i != xCell; ++i){
		for(int j = 0; j != yCell; ++j){
			std::cout << i << "," << j << ":" << dsv(cells[i][j]) << std::endl;
		}
	}
    
}

std::list<gridIndex> Grid::findIntersectionCells(polygon geom){
    
    boost::geometry::model::box<point> box;
    boost::geometry::envelope(geom, box);
	std::list<gridIndex> intersectionCells;
    
	for(int i = 0; i < xCell; ++i){
		for(int j = 0; j < yCell; ++j){
			bool b = boost::geometry::intersects(cells[i][j], box);
			if( b){
				gridIndex ints_cell(i,j);
				intersectionCells.push_back(ints_cell);
			}
		}
	}
    
	return intersectionCells;
    
}


Grid::~Grid() {
}