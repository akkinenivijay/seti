/*
 * FrontLine.h
 *
 *  Created on: Apr 20, 2014
 *      Author: vijay.akkineni
 */

#ifndef FRONTLINE_H_
#define FRONTLINE_H_

#include <stddef.h>  // defines NULL
#include <iostream>
#include <boost/unordered_set.hpp>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/io/wkt/wkt.hpp>
#include <boost/functional/hash.hpp>
#include <boost/progress.hpp>
#include <boost/timer.hpp>
#include "Instance.h"
#include "Grid.h"

using namespace std;
using namespace boost::geometry;
using boost::timer;
using boost::progress_timer;
using boost::progress_display;

typedef boost::unordered_set<Instance> unorderedFeatureSet;

class FrontLine {
public:
	static FrontLine* getInstance();
    FrontLine (const FrontLine&) = delete;
    FrontLine& operator=(const FrontLine&) = delete;
	void addFeatureInstance(Instance &inst,Grid & grid);
	void printHashSet();
    double insertionTime = 0;
    double intersectionTime = 0;
	double numberOfInstancesInserted = 0;
private:
	FrontLine() {
	}
	;
	~FrontLine() {
	}
	;
	static FrontLine* frontlineInstance;
};

#endif /* FRONTLINE_H_ */
