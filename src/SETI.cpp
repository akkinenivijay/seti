#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/unordered_set.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/unordered_set.hpp>
#include <boost/chrono.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/progress.hpp>
#include <boost/timer.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/mapped_file.hpp>
#include <istream>

#include <stx/btree.h>
#include <stx/btree_map.h>


#include <stdexcept>
#include "Instance.h"
#include "FrontLine.h"
#include "Grid.h"

using namespace std;
using namespace boost::filesystem;
using namespace boost::geometry;
using namespace stx;
using namespace boost::iostreams;

using boost::timer;
using boost::progress_timer;
using boost::progress_display;

typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

double d1 = 100000.0;
double d2 = 100000.0;
int xCell = 200;
int yCell = 200;
Grid grid(d1, d2, xCell, yCell);


int BuildSETI(path inputPath);


int BuildSETI(path inputPath){
    
	boost::char_separator<char> sep("-;|\f\n\r\t\v");
	boost::regex re;
    
	if (!is_directory(inputPath)) {
		cout << inputPath << " is not a directory!" << endl;
		return 0;
	}
    
	cout << "processing files from the directory: " << inputPath << endl;
    
	if(exists(inputPath)){
        boost::timer iotimer;
		directory_iterator end;
		for (directory_iterator it(inputPath); it!= end; ++it) {
			if(!is_directory(*it))
			{
				if(is_regular_file(*it)){
					path fileName = it -> path().filename();
					path filePath = it -> path();
                    
					try
					{
						boost::regex re("feature_[0-9]*"); // Checks if the file names matches the pattern feature_*
						if (boost::regex_match(fileName.string(), re))
						{
							string line;
                            stream<mapped_file_source> myfile;
							myfile.open(
                                        mapped_file_source(filePath.string()));
							if (myfile.is_open())
							{
								while ( getline (myfile,line) )
								{
									int index =0;
									int instanceId = 0, time = 0;
									string wkt;
									tokenizer tokens(line, sep);
									for (tokenizer::iterator tok_iter = tokens.begin();
                                         tok_iter != tokens.end(); ++tok_iter){
										if (index == 0) {
											instanceId = boost::lexical_cast<int>(*tok_iter);
										}
                                        
										if(index == 1){
											time = boost::lexical_cast<int>(*tok_iter);
										}
                                        
										if(index == 2){
											wkt = *tok_iter;
										}
                                        
										index++;
									}
									Instance inst(fileName.string(), instanceId, time, wkt);
									FrontLine::getInstance()->addFeatureInstance(inst,grid);
								}
								myfile.close();
							}
						}
					}
					catch (boost::regex_error& e)
					{
						cerr << "The regexp " << re << " is invalid!" << endl;
					}
                    
				}
			}
		}
        
        double elapsed_time = iotimer.elapsed();
        
		cout << "Total Time(IO+ Grid Intersection + Index Insertion): " << elapsed_time << endl;
		cout << "Pure IO Time reading file instances: " << elapsed_time - FrontLine::getInstance()->insertionTime - FrontLine::getInstance()->intersectionTime << endl;
        
	}else{
		cout << inputPath << " does not exist, Please check the input path!!!" << endl;
	}
	return 0;
    
}

void spatial_windowQuery(box window){
    
	std::list<gridIndex> intersectionCells;
    
	for(int i = 0; i < xCell; ++i){
		for(int j = 0; j < yCell; ++j){
			bool b = boost::geometry::intersects(grid.cells[i][j], window);
			if( b){
				gridIndex ints_cell(i,j);
				intersectionCells.push_back(ints_cell);
			}
		}
	}
    
	for(const auto& elem : intersectionCells){
        if(grid.bptrees[elem.x()][elem.y()].lower_bound(0) != grid.bptrees[elem.x()][elem.y()].end()){
            bp_tree::iterator it = grid.bptrees[elem.x()][elem.y()].lower_bound(0);
            
            Instance i = it.data();
            while(true){
                
                if(boost::geometry::intersects(i.polygon, window)){
                    //i.printInstance();
                }
                it++;
                if( it == grid.bptrees[elem.x()][elem.y()].end()){
                    break;
                }
                i = it.data();
                
            }
        }
        
        
	}
}


void ST_windowQuery(polygon window, int timeStart, int timeEnd){
    
	std::list<gridIndex> intersectionCells;
    
	for(int i = 0; i < xCell; ++i){
		for(int j = 0; j < yCell; ++j){
			bool b = boost::geometry::intersects(grid.cells[i][j], window);
			if( b){
				gridIndex ints_cell(i,j);
				intersectionCells.push_back(ints_cell);
			}
		}
	}
    
	for(const auto& elem : intersectionCells){
        if(grid.bptrees[elem.x()][elem.y()].lower_bound(timeStart)!=grid.bptrees[elem.x()][elem.y()].end()){
            bp_tree::iterator it = grid.bptrees[elem.x()][elem.y()].lower_bound(timeStart);
            if(it.key() <= timeEnd){
                Instance i = it.data();
                while(i.time <= timeEnd){
                    if(boost::geometry::intersects(i.polygon, window)){
                        //i.printInstance();
                        
                    }
                    it++;
                    i = it.data();
                }
            }
        }
        
	}
}

void ST_IntersectionQuery(std::list<polygon> polygons, std::list<int> timestamps){
    
	if(polygons.size() != timestamps.size()){
        
		std::cout << "The size of polygons and timestamps are not matching" << std::endl;
		return;
	} else if (polygons.size() < 1){
		std::cout << "The size of query lists are less than or equal to zero! " << std::endl;
		return;
	} else {
        
		std::list<polygon>::const_iterator p_iter = polygons.begin();
		std::list<int>::const_iterator t_iter = timestamps.begin();
        
		while( p_iter != polygons.end()){
            
			ST_windowQuery(*p_iter, *t_iter, *t_iter);
            
			p_iter++;
			t_iter++;
		}
	}
    
    
}


void timeRangeQuery(int timeStart, int timeEnd){
	for(int i = 0; i < xCell; i++){
		for(int j = 0 ; j < yCell; j++){
            
            for (bp_tree::iterator it = grid.bptrees[i][j].lower_bound(timeStart) ; it != grid.bptrees[i][j].upper_bound(timeEnd) ;it++){
                Instance i = it.data();
                //i.printInstance();
            }
            
		}
	}
}

void timeStampQuery(int timeStamp){
	for(int i = 0; i < xCell-1; i++){
		for(int j = 0 ; j < yCell-1; j++){
            
            if(grid.bptrees[i][j].lower_bound(timeStamp) != grid.bptrees[i][j].end()){
                
                if(grid.bptrees[i][j].lower_bound(timeStamp).key() == timeStamp){
                    bp_tree::iterator it = grid.bptrees[i][j].find(timeStamp);
                    
                    Instance i = it.data();
                    while(i.time == timeStamp){
                        //i.printInstance();
                        it++;
                        i = it.data();
                    }
                }
            }
            
        }
    }
}

void parseQueryFromFile(path queryFilePath) {
    boost::char_separator<char> sep("-;|\f\n\r\t\v");
    
    int startTime = 0;
    int endTime = 0;
    
    std::ifstream myfile(queryFilePath.string());
    
    bool first = true;
    
    std::list<polygon> polygon_list;
    std::list<int> timestamps_list;
    int prevQueryId = -1;
    
    double timeStampSum, timeRangeSum, spatialWindowSum,
    ST_TimestampSum, ST_Intersection_Sum;
    int count = 0;
    
    if (myfile.is_open()) {
        string line;
        while (getline(myfile, line)) {
            int index = 0;
            int queryId = 0, time = 0;
            string wkt;
            tokenizer tokens(line, sep);
            for (tokenizer::iterator tok_iter = tokens.begin();
                 tok_iter != tokens.end(); ++tok_iter) {
                if (index == 0) {
                    queryId = boost::lexical_cast<int>(*tok_iter);
                }
                
                if (index == 1) {
                    time = boost::lexical_cast<int>(*tok_iter);
                }
                
                if (index == 2) {
                    wkt = *tok_iter;
                }
                
                index++;
            }
            
            if (first) {
                startTime = time;
                first = false;
            }
            
            endTime = time;
            
            polygon qPolygon;
            read_wkt(wkt, qPolygon);
            
            polygon_list.push_back(qPolygon);
            timestamps_list.push_back(time);
            
            boost::chrono::high_resolution_clock::time_point t1;
            boost::chrono::high_resolution_clock::time_point t2;
            if(prevQueryId == -1){
                prevQueryId = queryId;
            } else if(queryId != prevQueryId){
                
                
                //Temporal timestamp query
                boost::timer t1;
                timeStampQuery(time);
                double delay = t1.elapsed();
                timeStampSum = timeStampSum + delay;
                
                //Temporal time range query
                boost::timer t2;
                timeRangeQuery(time - timestamps_list.size(), time);
                delay = t2.elapsed();
                timeRangeSum = timeRangeSum + delay;
                
                //Spatial window query
                box window;
                boost::geometry::envelope(qPolygon, window);
                boost::timer t3;
                spatial_windowQuery(window);
                delay = t3.elapsed();
                spatialWindowSum = spatialWindowSum + delay;
                
                
                //Spatiotemporal timestamp query
                boost::timer t4;
                ST_windowQuery(qPolygon, time, time);
                delay = t4.elapsed();
                ST_TimestampSum = ST_TimestampSum + delay;
                
                //spatiotemporal intersection query
                boost::timer t5;
                ST_IntersectionQuery(polygon_list, timestamps_list);
                delay = t5.elapsed();
                ST_Intersection_Sum = ST_Intersection_Sum + delay;
                
                count++;
                
                polygon_list.clear();
                timestamps_list.clear();
                
                prevQueryId = queryId;
            }
            
            if(count == 200){
                myfile.close();
                break;
            }
            
        }
        myfile.close();
    }
    double avgSTIntersection = (double)ST_Intersection_Sum / (double)count;
    double avgST_Timestamp = (double) ST_TimestampSum / count;
    double avgTimeStamp = (double)timeStampSum / count;
    double avgTimeRange = (double)timeRangeSum / count;
    double avgSpatialWindow = (double) spatialWindowSum / count;
    
    cout<< "ST_Intersection_Avg: " << avgSTIntersection << endl;
    
    cout<< "ST_TimeStamp_Avg: " << avgST_Timestamp << endl;
    
    cout<< "Timestamp_Avg: " << avgTimeStamp << endl;
    
    cout<< "Timerange_Avg: " << avgTimeRange<< endl;
    
    cout<< "Sp_Window_Avg: " << avgSpatialWindow<< endl;
    
}

void queries(){
    
    timeStampQuery(100);
    
    timeRangeQuery(100, 200);
    
    point ul(0.0, 0.0);
    point lr(50000.0, 50000.0 );
    box window(ul, lr);
    
    polygon pwindow;
    boost::geometry::convert(window, pwindow);
    
    ST_windowQuery(pwindow, 100, 200);
    
    spatial_windowQuery(window);
    
    timeStampQuery(21);
}

int main(int argc, char *argv[])
{
    
    try
    {
        if (argc < 2)
        {
            cout << "Please provide a path to the directory where the feature files are present \n";
            return 1;
        }
        
        path inputPath = system_complete(path(argv[1]));
        path queryFile = system_complete(path(argv[2]));;
        
        BuildSETI(inputPath);
        
        cout << "Total Insertion Time to the BTree: " << FrontLine::getInstance()->insertionTime << endl;
        cout << "Total Grid Intersection Time for polygon grid intersection: " << FrontLine::getInstance()->intersectionTime << endl;
        cout << "Total NUmber of Instances inserted: " << FrontLine::getInstance()->numberOfInstancesInserted << endl;
        cout << "Average Insertion time: " << (FrontLine::getInstance()->insertionTime
            / FrontLine::getInstance()->numberOfInstancesInserted)
        << endl;
        
        cout << queryFile << endl;
        parseQueryFromFile(queryFile);
        
        //queries();
        
    }
    catch (std::exception e)
    {
        cout << "Unexpected exception running SETI " << endl;
    }
    
    return 0;
}