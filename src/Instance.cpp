/*
 * feature.cpp
 *
 *  Created on: Apr 19, 2014
 *      Author: Vijay Akkineni
 */

#include "Instance.h"

using namespace std;
using namespace boost::geometry;

typedef boost::geometry::model::polygon<boost::geometry::model::d2::point_xy<double> > polygonType;

Instance::Instance()
{
    featureID = "Uknown";
    instanceID = 0;
    time = 0;
}

Instance::Instance(std::string _featureID, int _instanceID, int _time, string _polygonWKT)
{
    featureID = _featureID;
    instanceID = _instanceID;
    time = _time;
    read_wkt(_polygonWKT, polygon);
    correct(polygon);
}

void Instance::printInstance(){
    
    std::cout << featureID << "-" <<instanceID << "-" << time  << endl;
    
}

int Instance::getInstanceID()
{
    return instanceID;
}

const string Instance::getFeatureID()
{
    return featureID;
}

int Instance::getTime()
{
    return time;
}

boost::geometry::model::polygon<boost::geometry::model::d2::point_xy<double> > Instance::getPolygon(){
    return polygon;
}